module.exports = {
  purge: [
    './dist/**/*.html'
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      screens: {
        'lappy': '1366px',
      },
      colors: {
        'creatify-darker-gray': '#a7a9ac',
        'creatify-aqua': '#E4F0F7',
        'creatify-purple': '#510A83',
        'creatify-cream': '#FBF0F4',
        'creatify-blue': '#6FB3F4',
        'creatify-pink': '#F5B4D6',
        'creatify-light': '#F1F1F1',
        'creatify-gray': '#cbcbcb',
        'creatify-dark': '#4D4D4D',
        'creatify-bg': '#fafbfc',
      },
      fontFamily: {
        'poppins': ['Poppins', 'sans-serif'],
      },
      fontSize: {
        '2xs': ['0.65rem', '0.75rem'],
        '5-5xl': ['3.25rem', '1']
      },
      spacing: {
        '18': '4.5rem',
        '26': '6.5rem',
        '68': '17rem',
        '76': '19rem',
        '88': '22rem',
        '100': '25rem',
        '104': '26rem',
        '108': '27rem',
        '120': '30rem',
        '132': '33rem',
        '140': '35rem',
        '160': '40rem',
        '168': '42rem',
        '200': '50rem',
        '216': '54rem',
        '232': '58rem',
        '260': '65rem',
      },
      padding: {
        'full': '100%',
        '1/2': '50%',
      },
      scale: {
        '45': '.45',
        '65': '.65',
      },
      borderRadius: {
        '4xl': '2.5rem',
      },
      gridTemplateRows: {
        '10': 'repeat(10, minmax(0, 1fr));'
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
