# Creatify HTML Project

Project for slicing web design to html

## Stack

+ [Tailwind CSS](https://tailwindcss.com/)
+ [AlpineJs](https://github.com/alpinejs/alpine)

## Clone and Run

1. `git clone git@gitlab.com:lebah-dev/creatify-html.git`
2. `cd creatify-html`
3. `npm install`
4. `npm run prod`

## Development

1. `npm run watch` or `npm run dev` so all tailwind css classes is accessable
2. after make some changes build it before push to git
3. `npm run prod`

## Deployment

1. copy all files and folders in `dist` directory to public path in the server